"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CoroutinePool = /** @class */ (function () {
    function CoroutinePool() {
    }
    CoroutinePool.UpdateAll = function () {
        for (var i = 0; i < this.crtns.length; i++) {
            var done = this.crtns[i].generator.next().done;
            if (done) {
                if (this.Logs)
                    console.log("Coroutine end. Handler " + this.crtns[i].handler);
                this.rmCrtn(i);
            }
        }
    };
    CoroutinePool.StartCoroutine = function (func, ctx) {
        var crtn = { generator: null, generatorWarpper: null, handler: -1 };
        crtn.handler = this.lastHandler;
        this.lastHandler++;
        crtn.generatorWarpper = func;
        if (ctx)
            crtn.generator = func(ctx);
        else
            crtn.generator = func();
        this.crtns.push(crtn);
        return crtn.handler;
    };
    CoroutinePool.StopCoroutine = function (handler) {
        for (var i = 0; i < this.crtns.length; i++) {
            if (this.crtns[i].handler === handler) {
                this.rmCrtn(i);
                return;
            }
        }
    };
    CoroutinePool.rmCrtn = function (index) {
        this.crtns.splice(index, 1);
    };
    CoroutinePool.Logs = false;
    CoroutinePool.crtns = [];
    CoroutinePool.lastHandler = 0;
    return CoroutinePool;
}());
exports.CoroutinePool = CoroutinePool;

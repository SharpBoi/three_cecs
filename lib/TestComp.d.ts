import { Behavior } from "./Behavior";
export default class TestComp extends Behavior {
    private test;
    Start: (args: any) => void;
    Update: () => void;
    OnDestroy: () => void;
}

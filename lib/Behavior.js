"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var three_1 = require("three");
var BehaviorPool_1 = require("./BehaviorPool");
/**
 * Parent class for all your components (like MonoBehavior)
 */
var Behavior = /** @class */ (function () {
    function Behavior(constrArgs) {
        var _this = this;
        this.started = false;
        this.typeName = "unknown";
        this.destroyed = false;
        this.enabled = true;
        /** Called once, before update */
        this.Start = undefined;
        /** Called every update iteration */
        this.Update = undefined;
        /** Called when components wants to be destroyed */
        this.OnDestroy = undefined;
        this.uid = generateId(Behavior.UID_LEN);
        this.typeName = constrArgs.typeName;
        this.setHostGameObject(constrArgs.hostGameObject);
        // start
        constrArgs.lifeHandler.start = function () {
            if (_this.started === false && _this.Start && _this.destroyed === false) {
                _this.started = true;
                _this.Start(constrArgs.startArgs);
            }
        };
        // update
        constrArgs.lifeHandler.update = function () {
            if (_this.started === false && _this.Start && _this.destroyed === false) {
                _this.started = true;
                _this.Start(constrArgs.startArgs);
            }
            if (_this.Update && _this.enabled && _this.gameObj.visible && _this.destroyed === false) {
                _this.Update();
            }
        };
        // destroy self
        constrArgs.lifeHandler.destroy = function () {
            if (_this.OnDestroy)
                _this.OnDestroy();
            _this.destroyed = true;
        };
    }
    Object.defineProperty(Behavior.prototype, "Destroyed", {
        get: function () { return this.destroyed; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Behavior.prototype, "TypeName", {
        get: function () { return this.typeName; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Behavior.prototype, "Started", {
        get: function () { return this.started; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Behavior.prototype, "UID", {
        get: function () { return this.uid; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Behavior.prototype, "gameObject", {
        get: function () { return this.gameObj; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Behavior.prototype, "userData", {
        get: function () {
            return this.getUserDataOf(this.gameObj);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Behavior.prototype, "localPos", {
        get: function () { return this.gameObject.position; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Behavior.prototype, "worldPos", {
        get: function () { return this.gameObj.localToWorld(new three_1.Vector3()); },
        set: function (v) { this.gameObject.position = this.gameObject.parent.worldToLocal(v.clone()); },
        enumerable: true,
        configurable: true
    });
    ;
    Behavior.prototype.AddComponent = function (beh, go) {
        if (go === void 0) { go = this.gameObj; }
        var res = BehaviorPool_1.BehaviorPool.InstantiateBehavior(go, beh, []);
        return res;
    };
    Behavior.prototype.DestroyComponent = function (instance) {
        this.removeBehFromGameObject(instance.gameObject, instance);
        BehaviorPool_1.BehaviorPool.DestroyBehavior(instance);
    };
    /**
     * Search component of given type on given object
     * @param beh type of behavior search to
     * @param go object to search in. Default - host of this component
     * @returns found component or undef
     */
    Behavior.prototype.GetComponent = function (beh, go) {
        if (go === void 0) { go = this.gameObj; }
        var typeIndex = this.getIndexOfTypeInBehsIds(go, beh);
        if (typeIndex === -1)
            return undefined;
        if (this.userData.behsIds[typeIndex].Array.length === 0)
            return undefined;
        return BehaviorPool_1.BehaviorPool.GetInstancesByUIDs(beh, [this.userData.behsIds[typeIndex].Array[0]])[0];
    };
    /**
     * Search components of given type in given object
     * @param beh type of behavior search to
     * @param go object to search in. Default - host of this component
     * @returns array of components, or empty array. NEVER UNDEF
     */
    Behavior.prototype.GetComponents = function (beh, go) {
        if (go === void 0) { go = this.gameObj; }
        var typeIndex = this.getIndexOfTypeInBehsIds(go, beh);
        if (typeIndex === -1)
            return undefined;
        if (this.userData.behsIds[typeIndex].Array.length === 0)
            return undefined;
        return BehaviorPool_1.BehaviorPool.GetInstancesByUIDs(beh, this.userData.behsIds[typeIndex].Array);
    };
    /**
     * Search component of given type, in childs of given object. Uses width first search
     * @param beh type of behavior search to
     * @param go object to start search in. Default - host of this component
     * @returns found component or undef
     */
    Behavior.prototype.GetComponentInChildren = function (beh, go) {
        if (go === void 0) { go = this.gameObj; }
        var queue = [];
        queue.push(go);
        while (queue.length !== 0) {
            // look
            var o = queue.shift();
            var res = this.GetComponent(beh, o);
            if (res)
                return res;
            // push childs
            queue = queue.concat(o.children);
        }
        return undefined;
    };
    /**
     * Search component of given type, in parent of given object. Uses width first search
     * @param beh type of behavior search to
     * @param go object to start search in. Default - host of this component
     * @returns found component or undef
     */
    Behavior.prototype.GetComponentInParent = function (beh, go) {
        var _this = this;
        if (go === void 0) { go = this.gameObj; }
        var res = undefined;
        go.traverseAncestors(function (o) {
            if (res === undefined) {
                res = _this.GetComponent(beh, o);
            }
        });
        return res;
    };
    /**
     * Search components of given type in children of given object
     * @param beh type of behavior search to
     * @param go object to start search in. Default - host of this component
     * @returns array of components, or empty array. NEVER UNDEF
     */
    Behavior.prototype.GetComponentsInChildren = function (beh, go) {
        if (go === void 0) { go = this.gameObj; }
        var res = [];
        var queue = [];
        queue.push(go);
        while (queue.length !== 0) {
            // look
            var o = queue.shift();
            var comps = this.GetComponents(beh, o);
            res = res.concat(comps);
            // push childs
            queue = queue.concat(o.children);
        }
        return res;
    };
    /**
     * Search components of given type in parents of given object
     * @param beh type of behavior search to
     * @param go object to start search in. Default - host of this component
     * @returns array of components, or empty array. NEVER UNDEF
     */
    Behavior.prototype.GetComponentsInParent = function (beh, go) {
        var _this = this;
        if (go === void 0) { go = this.gameObj; }
        var res = [];
        go.traverseAncestors(function (o) {
            res = res.concat(_this.GetComponents(beh, o));
        });
        return res;
    };
    /**
     * Moves this component on another hosting Object3D
     * @param obj new hosting Object3D
     */
    Behavior.prototype.setHostGameObject = function (obj) {
        // init behs ids if not exists
        this.safeInitBehsIds(obj);
        if (this.gameObj !== undefined)
            this.safeInitBehsIds(this.gameObj);
        // remove this comp from previous object, if it is
        if (this.gameObj !== undefined) {
            this.removeBehFromGameObject(this.gameObj, this);
        }
        // set host gameobject
        this.gameObj = obj;
        // search if type of this beh exists in list
        var typeIndex = this.getIndexOfTypeInBehsIds(this.gameObj, this.getTypeOfInst(this));
        if (typeIndex !== -1)
            this.userData.behsIds[typeIndex].Array.push(this.UID);
        else {
            // if there is no array of this type - create new and add
            var arr = new BehaviorPool_1.TypedArray(this.getTypeOfInst(this));
            arr.Array.push(this.UID);
            this.userData.behsIds.push(arr);
            return;
        }
    };
    Behavior.prototype.getUserDataOf = function (g) {
        this.safeInitBehsIds(g);
        return g.userData;
    };
    Behavior.prototype.safeInitBehsIds = function (g) {
        if (g.userData.behsIds === undefined) {
            g.userData.behsIds = [];
        }
    };
    Behavior.prototype.removeBehFromGameObject = function (g, instance) {
        var ud = this.getUserDataOf(g);
        for (var i = 0; i < ud.behsIds.length; i++)
            // if exists array of this type
            if (ud.behsIds[i].type === this.getTypeOfInst(instance)) {
                for (var j = 0; j < ud.behsIds[i].Array.length; j++) {
                    // if typed array contains comp with this uid
                    if (ud.behsIds[i].Array[j] === instance.UID) {
                        // remove this uid from array of this type
                        ud.behsIds[i].Array.splice(j, 1);
                        // if array of this type is empty - remove typed array
                        if (ud.behsIds[i].Array.length === 0)
                            ud.behsIds.splice(i, 1);
                        break;
                    }
                }
            }
    };
    Behavior.prototype.getIndexOfTypeInBehsIds = function (g, type) {
        var ud = this.getUserDataOf(g);
        for (var i = 0; i < ud.behsIds.length; i++) {
            if (ud.behsIds[i].type === type) {
                return i;
            }
        }
        return -1;
    };
    Behavior.prototype.getTypeOfInst = function (beh) {
        return Object.getPrototypeOf(beh).constructor;
    };
    Behavior.UID_LEN = 32;
    return Behavior;
}());
exports.Behavior = Behavior;
// util
var abc = "qwertyuiopasdfghjklzxcvbnm0123456789";
function generateId(len) {
    var res = "";
    for (var i = 0; i < len; i++)
        res += abc[Math.round((Math.random() * (abc.length - 1)))];
    return res;
}

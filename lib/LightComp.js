"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Behavior_1 = require("./Behavior");
var LightComp = /** @class */ (function (_super) {
    __extends(LightComp, _super);
    function LightComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.ligt = 123;
        _this.Start = function () {
            console.log("light comp start " + _this.ligt);
        };
        _this.Update = function () {
            // console.log(`update LightComp  ${this.ligt}`)
        };
        _this.OnDestroy = function () {
            console.log("Light comp destroy  " + _this.ligt);
        };
        return _this;
    }
    return LightComp;
}(Behavior_1.Behavior));
exports.default = LightComp;

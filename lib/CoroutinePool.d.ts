export declare class CoroutinePool {
    static Logs: boolean;
    private static crtns;
    private static lastHandler;
    static UpdateAll(): void;
    static StartCoroutine<Tctx>(func: (ctx?: Tctx) => IterableIterator<any>, ctx?: Tctx): number;
    static StopCoroutine(handler: number): void;
    private static rmCrtn;
}
export declare type Coroutine = {
    generator: IterableIterator<any>;
    generatorWarpper: () => IterableIterator<any>;
    handler: number;
};

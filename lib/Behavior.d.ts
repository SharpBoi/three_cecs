import { Object3D, Vector3 } from "three";
import { TypedArray, BehaviorLifecycleHandler } from "./BehaviorPool";
export declare type BehaviorConstructorArgs = {
    hostGameObject: Object3D;
    startArgs: any;
    lifeHandler: BehaviorLifecycleHandler;
    typeName: string;
};
/**
 * Parent class for all your components (like MonoBehavior)
 */
export declare class Behavior {
    static readonly UID_LEN = 32;
    private gameObj;
    private uid;
    private started;
    private typeName;
    private destroyed;
    enabled: boolean;
    readonly Destroyed: boolean;
    readonly TypeName: string;
    readonly Started: boolean;
    readonly UID: string;
    readonly gameObject: Object3D;
    readonly userData: UserData;
    readonly localPos: Vector3;
    worldPos: Vector3;
    constructor(constrArgs: BehaviorConstructorArgs);
    AddComponent<Tbeh extends Behavior>(beh: typeof Behavior, go?: Object3D): Tbeh;
    DestroyComponent(instance: Behavior): void;
    /**
     * Search component of given type on given object
     * @param beh type of behavior search to
     * @param go object to search in. Default - host of this component
     * @returns found component or undef
     */
    GetComponent<Tbeh extends Behavior>(beh: typeof Behavior, go?: Object3D): Tbeh | undefined;
    /**
     * Search components of given type in given object
     * @param beh type of behavior search to
     * @param go object to search in. Default - host of this component
     * @returns array of components, or empty array. NEVER UNDEF
     */
    GetComponents<Tbeh extends Behavior>(beh: typeof Behavior, go?: Object3D): Tbeh[];
    /**
     * Search component of given type, in childs of given object. Uses width first search
     * @param beh type of behavior search to
     * @param go object to start search in. Default - host of this component
     * @returns found component or undef
     */
    GetComponentInChildren<Tbeh extends Behavior>(beh: typeof Behavior, go?: Object3D): Tbeh;
    /**
     * Search component of given type, in parent of given object. Uses width first search
     * @param beh type of behavior search to
     * @param go object to start search in. Default - host of this component
     * @returns found component or undef
     */
    GetComponentInParent<Tbeh extends Behavior>(beh: typeof Behavior, go?: Object3D): Tbeh;
    /**
     * Search components of given type in children of given object
     * @param beh type of behavior search to
     * @param go object to start search in. Default - host of this component
     * @returns array of components, or empty array. NEVER UNDEF
     */
    GetComponentsInChildren<Tbeh extends Behavior>(beh: typeof Behavior, go?: Object3D): Tbeh[];
    /**
     * Search components of given type in parents of given object
     * @param beh type of behavior search to
     * @param go object to start search in. Default - host of this component
     * @returns array of components, or empty array. NEVER UNDEF
     */
    GetComponentsInParent<Tbeh extends Behavior>(beh: typeof Behavior, go?: Object3D): Tbeh[];
    /**
     * Moves this component on another hosting Object3D
     * @param obj new hosting Object3D
     */
    setHostGameObject(obj: Object3D): void;
    protected getUserDataOf(g: Object3D): UserData;
    private safeInitBehsIds;
    private removeBehFromGameObject;
    private getIndexOfTypeInBehsIds;
    private getTypeOfInst;
    /** Called once, before update */
    protected Start?: (args: any[]) => void;
    /** Called every update iteration */
    protected Update?: () => void;
    /** Called when components wants to be destroyed */
    protected OnDestroy?: () => void;
}
export declare type UserData = {
    behsIds: TypedArray<typeof Behavior, string>[];
    [key: string]: any;
};

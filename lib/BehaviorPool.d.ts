import { Behavior } from "./Behavior";
import { Object3D, Scene } from "three";
export declare type BehaviorLifecycleHandler = {
    start: () => void;
    update: () => void;
    destroy: () => void;
};
declare type BehaviorLifeBundle = {
    behaviorInstance: Behavior;
    lifecycleHandler: BehaviorLifecycleHandler;
};
/**
 * Controls Behaviors life cycle
 */
export declare class BehaviorPool {
    private static allBehs;
    private static startableBehs;
    static Logs: boolean;
    static readonly AllBehaviors: TypedArray<typeof Behavior, BehaviorLifeBundle>[];
    /**
     * Callback for parsing arguments, when InstantiateComponents
     * @param args arguments string
     */
    static ArgumentsParser: (args: string) => string[];
    /**
     * Call it to start all instantiated Behaviors, before UpdateAll.
     * You can call it in your main update funciton, for automatizate call Start on every newly instantiated
     *
     * Example:
     * ```typescript
     * yourUpdateFunction() {
     *      BehaviorPool.StartAll();
     *      BehaviorPool.UpdateAll();
     * }
     * ```
     */
    static StartAll(): void;
    /**
     * Updates all started Behaviors. Use it in your main update function. After StartAll
     */
    static UpdateAll(): void;
    /**
     * Register your custom component inherited from Behavior
     * @param beh your component type
     */
    static RegisterBehavior(beh: typeof Behavior): void;
    /**
     * Create an instance of component of given type
     * @param gameObject hosting instance of Object3D
     * @param behTypeOrName type or name of component to instantiate
     * @param args arguments, to pass into Start of your component
     */
    static InstantiateBehavior<Tbeh extends Behavior>(gameObject: Object3D, behTypeOrName: typeof Behavior | string, args: any): Tbeh;
    /**
     * Instantiate all components on node or scene. May do it traverse.
     *
     * You can use it, to instantiate all component on node tree, writed in .userData like:
     * ```typescript
     * node.userData {
     *  Component0: "args",
     *  Component1: "args;example",
     *  Rotator: "axis: 0 1 0; speed: 1", //for example
     *  ...
     * }
     * ```
     * @param node node which components will be instantiated, or its children components too
     * @param traverse do traversal instantiation for childs of given node
     */
    static InstantiateComponents(node: Object3D | Scene, traverse?: boolean): void;
    /**
     * Destroy an instance of Behavior. Dont use it main case. Instead, use DestroyComponent in Behavior
     * @param instance
     */
    static DestroyBehavior(instance: Behavior): void;
    static GetInstancesByUIDs(type: typeof Behavior, uids: string[]): Behavior[];
}
export declare class TypedArray<Ttype, Tvar> {
    type: Ttype;
    Array: Tvar[];
    constructor(type?: Ttype);
}
export {};

"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var TestComp_1 = __importDefault(require("./TestComp"));
var BehaviorPool_1 = require("./BehaviorPool");
var LightComp_1 = __importDefault(require("./LightComp"));
var three_1 = require("three");
BehaviorPool_1.BehaviorPool.Logs = false;
BehaviorPool_1.BehaviorPool.RegisterBehavior(LightComp_1.default);
BehaviorPool_1.BehaviorPool.RegisterBehavior(TestComp_1.default);
BehaviorPool_1.BehaviorPool.ArgumentsParser = function (args) {
    return [args];
};
var testobj = new three_1.Object3D();
testobj.name = "test";
console.log("UD after create obj", testobj.userData.behsIds);
testobj.userData["LightComp"] = 1;
testobj.userData["TestComp"] = 1;
// inst comps
BehaviorPool_1.BehaviorPool.InstantiateComponents(testobj, true);
console.log("UD after inst comps", testobj.userData);
setInterval(function () {
    BehaviorPool_1.BehaviorPool.StartAll();
    BehaviorPool_1.BehaviorPool.UpdateAll();
}, 250);

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Behavior_1 = require("./Behavior");
var LightComp_1 = __importDefault(require("./LightComp"));
var TestComp = /** @class */ (function (_super) {
    __extends(TestComp, _super);
    function TestComp() {
        // public constructor(a: any) {
        //     super();
        // }
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.test = "sss";
        _this.Start = function (args) {
            console.log("TestComp start " + _this.test + " " + (_this instanceof LightComp_1.default));
            // this.AddComponent<LightComp>(LightComp);
            // this.AddComponent<LightComp>(LightComp);
            // setTimeout(() => {
            //     console.log(`UD before destroy comp`, this.userData.behsIds);
            //     this.DestroyComponent(this);
            //     console.log(`UD after destroy comp`, this.userData.behsIds);
            //     console.log(`COMP after destroy `, this);
            // }, 2000);
        };
        _this.Update = function () {
            // console.log(`update TestComp ${this.UID}`)
        };
        _this.OnDestroy = function () {
            console.log("Test comp destroy  " + _this.test);
        };
        return _this;
    }
    return TestComp;
}(Behavior_1.Behavior));
exports.default = TestComp;

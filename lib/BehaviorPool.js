"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Controls Behaviors life cycle
 */
var BehaviorPool = /** @class */ (function () {
    function BehaviorPool() {
    }
    Object.defineProperty(BehaviorPool, "AllBehaviors", {
        get: function () { return this.allBehs; },
        enumerable: true,
        configurable: true
    });
    /**
     * Call it to start all instantiated Behaviors, before UpdateAll.
     * You can call it in your main update funciton, for automatizate call Start on every newly instantiated
     *
     * Example:
     * ```typescript
     * yourUpdateFunction() {
     *      BehaviorPool.StartAll();
     *      BehaviorPool.UpdateAll();
     * }
     * ```
     */
    BehaviorPool.StartAll = function () {
        if (this.startableBehs.length > 0)
            for (var i = 0; i < this.allBehs.length; i++) {
                for (var j = 0; j < this.startableBehs.length; j++) {
                    if (this.startableBehs[j].behaviorInstance.TypeName === this.allBehs[i].type.name) {
                        this.startableBehs[j].lifecycleHandler.start();
                        this.startableBehs.splice(j, 1);
                    }
                }
            }
    };
    /**
     * Updates all started Behaviors. Use it in your main update function. After StartAll
     */
    BehaviorPool.UpdateAll = function () {
        for (var i = 0; i < this.allBehs.length; i++) {
            for (var j = 0; j < this.allBehs[i].Array.length; j++) {
                if (this.allBehs[i].Array[j].lifecycleHandler.update) {
                    this.allBehs[i].Array[j].lifecycleHandler.update();
                }
            }
        }
    };
    /**
     * Register your custom component inherited from Behavior
     * @param beh your component type
     */
    BehaviorPool.RegisterBehavior = function (beh) {
        // check is array of given bhe exists
        for (var i = 0; i < this.allBehs.length; i++) {
            if (this.allBehs[i].type.name === beh.name) {
                throw "Behavior " + beh.name + " alredy registered !!!";
            }
        }
        // if there is no array of given behs => add needed array
        this.allBehs.push(new TypedArray(beh));
        if (this.Logs)
            console.log("Register behavior " + beh.name);
    };
    /**
     * Create an instance of component of given type
     * @param gameObject hosting instance of Object3D
     * @param behTypeOrName type or name of component to instantiate
     * @param args arguments, to pass into Start of your component
     */
    BehaviorPool.InstantiateBehavior = function (gameObject, behTypeOrName, args) {
        // search for given type
        var searchName = typeof behTypeOrName === "string" ? behTypeOrName : behTypeOrName.name;
        var foundTypeIndex = -1;
        for (var i = 0; i < this.allBehs.length; i++) {
            if (this.allBehs[i].type.name === searchName) {
                foundTypeIndex = i;
                break;
            }
        }
        if (foundTypeIndex === -1) {
            throw "Try instantiate unregistered Behavior " + searchName + " !";
        }
        // get type of instantiable beh
        var type = this.allBehs[foundTypeIndex].type;
        // create lyfecycle handler
        var lyfecycleHandler = { start: null, update: null, destroy: null };
        // create instance of selected beh type
        var inst = new type({
            hostGameObject: gameObject,
            startArgs: args,
            lifeHandler: lyfecycleHandler,
            typeName: type.name
        }); // bottle neck, but works
        // add beh with its updater
        var lifeBundle = {
            behaviorInstance: inst,
            lifecycleHandler: lyfecycleHandler
        };
        this.allBehs[foundTypeIndex].Array.push(lifeBundle);
        this.startableBehs.push(lifeBundle);
        return inst;
    };
    /**
     * Instantiate all components on node or scene. May do it traverse.
     *
     * You can use it, to instantiate all component on node tree, writed in .userData like:
     * ```typescript
     * node.userData {
     *  Component0: "args",
     *  Component1: "args;example",
     *  Rotator: "axis: 0 1 0; speed: 1", //for example
     *  ...
     * }
     * ```
     * @param node node which components will be instantiated, or its children components too
     * @param traverse do traversal instantiation for childs of given node
     */
    BehaviorPool.InstantiateComponents = function (node, traverse) {
        var _this = this;
        if (traverse === void 0) { traverse = true; }
        if (traverse === false) {
            var keys = Object.keys(node.userData);
            for (var i = 0; i < this.AllBehaviors.length; i++) {
                for (var j = 0; j < keys.length; j++) {
                    if (keys[j] === this.AllBehaviors[i].type.name) {
                        var rawArgs = node.userData[keys[j]];
                        var parsedArgs = this.ArgumentsParser(rawArgs);
                        this.InstantiateBehavior(node, this.AllBehaviors[i].type, parsedArgs);
                    }
                }
            }
        }
        else {
            node.traverse(function (c) {
                var keys = Object.keys(c.userData);
                for (var i = 0; i < _this.AllBehaviors.length; i++) {
                    for (var j = 0; j < keys.length; j++) {
                        if (keys[j] === _this.AllBehaviors[i].type.name) {
                            var rawArgs = c.userData[keys[j]];
                            var parsedArgs = _this.ArgumentsParser(rawArgs);
                            _this.InstantiateBehavior(c, _this.AllBehaviors[i].type, parsedArgs);
                        }
                    }
                }
            });
        }
    };
    /**
     * Destroy an instance of Behavior. Dont use it main case. Instead, use DestroyComponent in Behavior
     * @param instance
     */
    BehaviorPool.DestroyBehavior = function (instance) {
        if (this.Logs)
            console.log(this.allBehs);
        var instConstr = Object.getPrototypeOf(instance).constructor;
        if (this.Logs)
            console.log(instConstr === this.allBehs[0].type);
        for (var i = 0; i < this.allBehs.length; i++) {
            if (instConstr === this.allBehs[i].type) {
                for (var j = 0; j < this.allBehs[i].Array.length; j++) {
                    if (this.allBehs[i].Array[j].behaviorInstance.UID === instance.UID) {
                        this.allBehs[i].Array[j].lifecycleHandler.destroy();
                        this.allBehs[i].Array.splice(j, 1);
                        if (this.Logs)
                            console.log("Destroyed beh " + instance.TypeName + " " + instance.UID);
                    }
                }
            }
        }
        if (this.Logs)
            console.log(this.allBehs);
    };
    BehaviorPool.GetInstancesByUIDs = function (type, uids) {
        var res = [];
        for (var i = 0; i < this.allBehs.length; i++)
            if (this.allBehs[i].type === type)
                for (var j = 0; j < this.allBehs[i].Array.length; j++)
                    if (uids.indexOf(this.allBehs[i].Array[j].behaviorInstance.UID) !== -1) {
                        res.push(this.allBehs[i].Array[j].behaviorInstance);
                    }
        return res;
    };
    BehaviorPool.allBehs = [];
    BehaviorPool.startableBehs = [];
    BehaviorPool.Logs = true;
    /**
     * Callback for parsing arguments, when InstantiateComponents
     * @param args arguments string
     */
    BehaviorPool.ArgumentsParser = function (args) { return []; };
    return BehaviorPool;
}());
exports.BehaviorPool = BehaviorPool;
var TypedArray = /** @class */ (function () {
    function TypedArray(type) {
        this.Array = [];
        this.type = type;
    }
    return TypedArray;
}());
exports.TypedArray = TypedArray;

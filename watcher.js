const fs = require("fs");
const path = require("path");
const exec = require("child_process");
const stripJsonComments = require("strip-json-comments");

let isWin = process.platform === "win32";
let tsconfig = JSON.parse(stripJsonComments(fs.readFileSync("./tsconfig.json").toString()));
let rootDir = tsconfig.compilerOptions.rootDir;
let outDir = tsconfig.compilerOptions.outDir;

let changed = false;
let procs = [];

fs.watch(path.join(__dirname, rootDir), (ev, file) => {
    changed = true;
    console.log("changed", ev, file);
});

setInterval(() => {
    if (changed) {
        changed = false;

        build();
    }

}, 250);

function build() {
    for (let i = 0; i < procs.length; i++) {
        procs[i].kill();
    }
    procs = [];

    console.log("building ... ")
    let tsc = exec.spawn("tsc" + (isWin ? ".cmd" : ""));
    procs.push(tsc);
    tsc.addListener("close", (code, sig) => {
        console.log("builded");

        console.log("running...");
        let node = exec.spawn("node", ["./lib/Test.js"]);
        procs.push(node);
        node.stdout.addListener("data", (chunk) => {
            let out = chunk.toString();
            out = out.substr(0, out.length - 1);
            console.log(out);
        });
    });
}
build();
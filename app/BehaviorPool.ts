import { Behavior, BehaviorConstructorArgs } from "./Behavior";
import { Object3D, Scene } from "three";
import TestComp from "./TestComp";

export type BehaviorLifecycleHandler = { start: () => void, update: () => void, destroy: () => void };
type BehaviorLifeBundle = { behaviorInstance: Behavior, lifecycleHandler: BehaviorLifecycleHandler }

/**
 * Controls Behaviors life cycle
 */
export class BehaviorPool {
    private static allBehs: TypedArray<typeof Behavior, BehaviorLifeBundle>[] = [];
    private static startableBehs: BehaviorLifeBundle[] = [];

    public static Logs = true;
    public static get AllBehaviors() { return this.allBehs }

    /** 
     * Callback for parsing arguments, when InstantiateComponents
     * @param args arguments string
     */
    public static ArgumentsParser: (args: string) => string[] = (args) => { return []; };

    /**
     * Call it to start all instantiated Behaviors, before UpdateAll.
     * You can call it in your main update funciton, for automatizate call Start on every newly instantiated 
     * 
     * Example: 
     * ```typescript
     * yourUpdateFunction() {
     *      BehaviorPool.StartAll();
     *      BehaviorPool.UpdateAll();
     * }
     * ```
     */
    public static StartAll() {
        if (this.startableBehs.length > 0)
            for (let i = 0; i < this.allBehs.length; i++) {
                for (let j = 0; j < this.startableBehs.length; j++) {
                    if (this.startableBehs[j].behaviorInstance.TypeName === this.allBehs[i].type.name) {
                        this.startableBehs[j].lifecycleHandler.start();
                        this.startableBehs.splice(j, 1);
                    }
                }
            }
    }

    /**
     * Updates all started Behaviors. Use it in your main update function. After StartAll
     */
    public static UpdateAll() {
        for (let i = 0; i < this.allBehs.length; i++) {
            for (let j = 0; j < this.allBehs[i].Array.length; j++) {
                if (this.allBehs[i].Array[j].lifecycleHandler.update) {
                    this.allBehs[i].Array[j].lifecycleHandler.update();
                }
            }
        }
    }

    /**
     * Register your custom component inherited from Behavior
     * @param beh your component type
     */
    public static RegisterBehavior(beh: typeof Behavior) {
        // check is array of given bhe exists
        for (let i = 0; i < this.allBehs.length; i++) {
            if (this.allBehs[i].type.name === beh.name) {
                throw `Behavior ${beh.name} alredy registered !!!`;
            }
        }

        // if there is no array of given behs => add needed array
        this.allBehs.push(new TypedArray(beh))
        if (this.Logs) console.log(`Register behavior ${beh.name}`);
    }

    /**
     * Create an instance of component of given type
     * @param gameObject hosting instance of Object3D
     * @param behTypeOrName type or name of component to instantiate
     * @param args arguments, to pass into Start of your component
     */
    public static InstantiateBehavior<Tbeh extends Behavior>(gameObject: Object3D, behTypeOrName: typeof Behavior | string, args: any) {
        // search for given type
        let searchName = typeof behTypeOrName === "string" ? behTypeOrName : behTypeOrName.name;
        let foundTypeIndex: number = -1;
        for (let i = 0; i < this.allBehs.length; i++) {
            if (this.allBehs[i].type.name === searchName) {
                foundTypeIndex = i;
                break;
            }
        }

        if (foundTypeIndex === -1) {
            throw `Try instantiate unregistered Behavior ${searchName} !`;
        }

        // get type of instantiable beh
        let type = this.allBehs[foundTypeIndex].type;

        // create lyfecycle handler
        let lyfecycleHandler: BehaviorLifecycleHandler = { start: null, update: null, destroy: null }

        // create instance of selected beh type
        let inst = new (type as any)({
            hostGameObject: gameObject,
            startArgs: args,
            lifeHandler: lyfecycleHandler,
            typeName: type.name
        } as BehaviorConstructorArgs) as Tbeh; // bottle neck, but works

        // add beh with its updater
        let lifeBundle = {
            behaviorInstance: inst,
            lifecycleHandler: lyfecycleHandler
        };
        this.allBehs[foundTypeIndex].Array.push(lifeBundle);
        this.startableBehs.push(lifeBundle);

        return inst;
    }

    /**
     * Instantiate all components on node or scene. May do it traverse.
     * 
     * You can use it, to instantiate all component on node tree, writed in .userData like:
     * ```typescript
     * node.userData { 
     *  Component0: "args",
     *  Component1: "args;example",
     *  Rotator: "axis: 0 1 0; speed: 1", //for example
     *  ... 
     * }
     * ```
     * @param node node which components will be instantiated, or its children components too
     * @param traverse do traversal instantiation for childs of given node
     */
    public static InstantiateComponents(node: Object3D | Scene, traverse = true) {
        if (traverse === false) {
            let keys = Object.keys(node.userData);
            for (let i = 0; i < this.AllBehaviors.length; i++) {
                for (let j = 0; j < keys.length; j++) {
                    if (keys[j] === this.AllBehaviors[i].type.name) {
                        let rawArgs = node.userData[keys[j]];
                        let parsedArgs = this.ArgumentsParser(rawArgs);

                        this.InstantiateBehavior(node, this.AllBehaviors[i].type, parsedArgs);
                    }
                }
            }
        }
        else {
            node.traverse(c => {
                let keys = Object.keys(c.userData);
                for (let i = 0; i < this.AllBehaviors.length; i++) {
                    for (let j = 0; j < keys.length; j++) {
                        if (keys[j] === this.AllBehaviors[i].type.name) {
                            let rawArgs = c.userData[keys[j]];
                            let parsedArgs = this.ArgumentsParser(rawArgs);

                            this.InstantiateBehavior(c, this.AllBehaviors[i].type, parsedArgs);
                        }
                    }
                }
            })
        }
    }

    /**
     * Destroy an instance of Behavior. Dont use it main case. Instead, use DestroyComponent in Behavior
     * @param instance 
     */
    public static DestroyBehavior(instance: Behavior) {
        if (this.Logs) console.log(this.allBehs);
        let instConstr = Object.getPrototypeOf(instance).constructor as typeof Behavior;
        if (this.Logs) console.log(instConstr === this.allBehs[0].type);

        for (let i = 0; i < this.allBehs.length; i++) {
            if (instConstr === this.allBehs[i].type) {
                for (let j = 0; j < this.allBehs[i].Array.length; j++) {
                    if (this.allBehs[i].Array[j].behaviorInstance.UID === instance.UID) {
                        this.allBehs[i].Array[j].lifecycleHandler.destroy();
                        this.allBehs[i].Array.splice(j, 1);

                        if (this.Logs)
                            console.log(`Destroyed beh ${instance.TypeName} ${instance.UID}`);
                    }
                }
            }
        }

        if (this.Logs) console.log(this.allBehs);
    }


    public static GetInstancesByUIDs(type: typeof Behavior, uids: string[]) {
        let res: Behavior[] = [];

        for (let i = 0; i < this.allBehs.length; i++)
            if (this.allBehs[i].type === type)
                for (let j = 0; j < this.allBehs[i].Array.length; j++)
                    if (uids.indexOf(this.allBehs[i].Array[j].behaviorInstance.UID) !== -1) {
                        res.push(this.allBehs[i].Array[j].behaviorInstance);
                    }

        return res;
    }


}

export class TypedArray<Ttype, Tvar> {
    public type: Ttype;
    public Array: Tvar[] = [];

    constructor(type?: Ttype) {
        this.type = type;
    }
}

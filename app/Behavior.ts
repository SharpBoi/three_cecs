import { Object3D, Vector3 } from "three"
import { TypedArray, BehaviorLifecycleHandler, BehaviorPool } from "./BehaviorPool";
import TestComp from "./TestComp";

export type BehaviorConstructorArgs = {
    hostGameObject: Object3D,
    startArgs: any,
    lifeHandler: BehaviorLifecycleHandler,
    typeName: string
}

/**
 * Parent class for all your components (like MonoBehavior)
 */
export class Behavior {
    public static readonly UID_LEN = 32;


    private gameObj: Object3D;
    private uid: string;
    private started = false;
    private typeName = "unknown";
    private destroyed = false;


    public enabled = true;


    public get Destroyed() { return this.destroyed }
    public get TypeName() { return this.typeName }
    public get Started() { return this.started }
    public get UID() { return this.uid }
    public get gameObject() { return this.gameObj }
    public get userData(): UserData {
        return this.getUserDataOf(this.gameObj);
    }

    public get localPos() { return this.gameObject.position }
    public get worldPos() { return this.gameObj.localToWorld(new Vector3()); }
    public set worldPos(v: Vector3) { this.gameObject.position = this.gameObject.parent.worldToLocal(v.clone()); }


    public constructor(constrArgs: BehaviorConstructorArgs) {
        this.uid = generateId(Behavior.UID_LEN);
        this.typeName = constrArgs.typeName;
        this.setHostGameObject(constrArgs.hostGameObject);

        // start
        constrArgs.lifeHandler.start = () => {
            if (this.started === false && this.Start && this.destroyed === false) {
                this.started = true;
                this.Start(constrArgs.startArgs);
            }
        }

        // update
        constrArgs.lifeHandler.update = () => {
            if (this.started === false && this.Start && this.destroyed === false) {
                this.started = true;
                this.Start(constrArgs.startArgs);
            }

            if (this.Update && this.enabled && this.gameObj.visible && this.destroyed === false) {
                this.Update();
            }
        }

        // destroy self
        constrArgs.lifeHandler.destroy = () => {
            if (this.OnDestroy)
                this.OnDestroy();
            this.destroyed = true;
        }
    };


    public AddComponent<Tbeh extends Behavior>(beh: typeof Behavior, go = this.gameObj): Tbeh {
        let res = BehaviorPool.InstantiateBehavior<Tbeh>(go, beh, []);
        return res;
    }
    public DestroyComponent(instance: Behavior) {
        this.removeBehFromGameObject(instance.gameObject, instance);
        BehaviorPool.DestroyBehavior(instance);
    }

    /**
     * Search component of given type on given object
     * @param beh type of behavior search to
     * @param go object to search in. Default - host of this component
     * @returns found component or undef
     */
    public GetComponent<Tbeh extends Behavior>(beh: typeof Behavior, go = this.gameObj): Tbeh | undefined {
        let typeIndex = this.getIndexOfTypeInBehsIds(go, beh);

        if (typeIndex === -1) return undefined;
        if (this.userData.behsIds[typeIndex].Array.length === 0) return undefined;

        return BehaviorPool.GetInstancesByUIDs(beh, [this.userData.behsIds[typeIndex].Array[0]])[0] as Tbeh;
    }
    /**
     * Search components of given type in given object
     * @param beh type of behavior search to
     * @param go object to search in. Default - host of this component
     * @returns array of components, or empty array. NEVER UNDEF
     */
    public GetComponents<Tbeh extends Behavior>(beh: typeof Behavior, go = this.gameObj): Tbeh[] {
        let typeIndex = this.getIndexOfTypeInBehsIds(go, beh);

        if (typeIndex === -1) return undefined;
        if (this.userData.behsIds[typeIndex].Array.length === 0) return undefined;

        return BehaviorPool.GetInstancesByUIDs(beh, this.userData.behsIds[typeIndex].Array) as Tbeh[];
    }

    /**
     * Search component of given type, in childs of given object. Uses width first search
     * @param beh type of behavior search to
     * @param go object to start search in. Default - host of this component 
     * @returns found component or undef
     */
    public GetComponentInChildren<Tbeh extends Behavior>(beh: typeof Behavior, go = this.gameObj): Tbeh {
        let queue: Object3D[] = [];
        queue.push(go);
        while (queue.length !== 0) {
            // look
            let o = queue.shift();
            let res = this.GetComponent<Tbeh>(beh, o);
            if (res) return res;

            // push childs
            queue = queue.concat(o.children);
        }

        return undefined;
    }
    /**
     * Search component of given type, in parent of given object. Uses width first search
     * @param beh type of behavior search to
     * @param go object to start search in. Default - host of this component 
     * @returns found component or undef
     */
    public GetComponentInParent<Tbeh extends Behavior>(beh: typeof Behavior, go = this.gameObj): Tbeh {
        let res = undefined as Tbeh;
        go.traverseAncestors(o => {
            if (res === undefined) {
                res = this.GetComponent(beh, o);
            }
        });

        return res;
    }

    /**
     * Search components of given type in children of given object
     * @param beh type of behavior search to
     * @param go object to start search in. Default - host of this component 
     * @returns array of components, or empty array. NEVER UNDEF
     */
    public GetComponentsInChildren<Tbeh extends Behavior>(beh: typeof Behavior, go = this.gameObj): Tbeh[] {
        let res: Tbeh[] = [];
        let queue: Object3D[] = [];

        queue.push(go);
        while (queue.length !== 0) {
            // look
            let o = queue.shift();
            let comps = this.GetComponents<Tbeh>(beh, o);
            res = res.concat(comps);

            // push childs
            queue = queue.concat(o.children);
        }

        return res;
    }
    /**
     * Search components of given type in parents of given object
     * @param beh type of behavior search to
     * @param go object to start search in. Default - host of this component
     * @returns array of components, or empty array. NEVER UNDEF
     */
    public GetComponentsInParent<Tbeh extends Behavior>(beh: typeof Behavior, go = this.gameObj): Tbeh[] {
        let res: Tbeh[] = [];
        go.traverseAncestors(o => {
            res = res.concat(this.GetComponents(beh, o));
        });

        return res;
    }

    /**
     * Moves this component on another hosting Object3D
     * @param obj new hosting Object3D
     */
    public setHostGameObject(obj: Object3D) {
        // init behs ids if not exists
        this.safeInitBehsIds(obj);
        if (this.gameObj !== undefined)
            this.safeInitBehsIds(this.gameObj);

        // remove this comp from previous object, if it is
        if (this.gameObj !== undefined) {
            this.removeBehFromGameObject(this.gameObj, this);
        }

        // set host gameobject
        this.gameObj = obj;

        // search if type of this beh exists in list
        let typeIndex = this.getIndexOfTypeInBehsIds(this.gameObj, this.getTypeOfInst(this));
        if (typeIndex !== -1)
            this.userData.behsIds[typeIndex].Array.push(this.UID);
        else {
            // if there is no array of this type - create new and add
            let arr = new TypedArray<typeof Behavior, string>(this.getTypeOfInst(this));
            arr.Array.push(this.UID);
            this.userData.behsIds.push(arr);
            return;
        }
    }
    protected getUserDataOf(g: Object3D): UserData {
        this.safeInitBehsIds(g);
        return g.userData as UserData;
    }

    private safeInitBehsIds(g: Object3D) {
        if (g.userData.behsIds === undefined) {
            g.userData.behsIds = [];
        }
    }
    private removeBehFromGameObject(g: Object3D, instance: Behavior) {
        let ud = this.getUserDataOf(g);
        for (let i = 0; i < ud.behsIds.length; i++)
            // if exists array of this type
            if (ud.behsIds[i].type === this.getTypeOfInst(instance)) {
                for (let j = 0; j < ud.behsIds[i].Array.length; j++) {
                    // if typed array contains comp with this uid
                    if (ud.behsIds[i].Array[j] === instance.UID) {
                        // remove this uid from array of this type
                        ud.behsIds[i].Array.splice(j, 1);
                        // if array of this type is empty - remove typed array
                        if (ud.behsIds[i].Array.length === 0)
                            ud.behsIds.splice(i, 1);
                        break;
                    }
                }
            }
    }
    private getIndexOfTypeInBehsIds(g: Object3D, type: typeof Behavior): number {
        let ud = this.getUserDataOf(g);
        for (let i = 0; i < ud.behsIds.length; i++) {
            if (ud.behsIds[i].type === type) {
                return i;
            }
        }
        return -1;
    }
    private getTypeOfInst(beh: Behavior) {
        return Object.getPrototypeOf(beh).constructor as typeof Behavior;
    }

    /** Called once, before update */
    protected Start?: (args: any[]) => void = undefined;
    /** Called every update iteration */
    protected Update?: () => void = undefined;
    /** Called when components wants to be destroyed */
    protected OnDestroy?: () => void = undefined;
}

export type UserData = { behsIds: TypedArray<typeof Behavior, string>[], [key: string]: any };

// util
let abc = "qwertyuiopasdfghjklzxcvbnm0123456789";
function generateId(len: number) {
    let res = "";
    for (let i = 0; i < len; i++)
        res += abc[Math.round((Math.random() * (abc.length - 1)))];
    return res;
}
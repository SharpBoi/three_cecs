import TestComp from "./TestComp";
import { BehaviorPool } from "./BehaviorPool";
import LightComp from "./LightComp";
import { Object3D as Object3DThree } from "three";
import { Behavior } from "./Behavior";

BehaviorPool.Logs = false;
BehaviorPool.RegisterBehavior(LightComp);
BehaviorPool.RegisterBehavior(TestComp);
BehaviorPool.ArgumentsParser = (args) => { // stub
    return [args];
}

let testobj = new Object3DThree(); testobj.name = "test";
console.log(`UD after create obj`, testobj.userData.behsIds);
testobj.userData["LightComp"] = 1;
testobj.userData["TestComp"] = 1;



// inst comps
BehaviorPool.InstantiateComponents(testobj, true);


console.log(`UD after inst comps`, testobj.userData);



setInterval(() => {
    BehaviorPool.StartAll();
    BehaviorPool.UpdateAll();
}, 250);




export class CoroutinePool {

    public static Logs = false;


    private static crtns: Coroutine[] = [];
    private static lastHandler = 0;

    public static UpdateAll() {
        for (let i = 0; i < this.crtns.length; i++) {
            let done = this.crtns[i].generator.next().done;
            if (done) {
                if (this.Logs)
                    console.log(`Coroutine end. Handler ${this.crtns[i].handler}`);
                this.rmCrtn(i);
            }
        }
    }

    public static StartCoroutine<Tctx>(func: (ctx?: Tctx) => IterableIterator<any>, ctx?: Tctx) {
        let crtn = { generator: null, generatorWarpper: null, handler: -1 } as Coroutine;
        crtn.handler = this.lastHandler;
        this.lastHandler++;
        crtn.generatorWarpper = func;
        if (ctx) crtn.generator = func(ctx);
        else crtn.generator = func();
        this.crtns.push(crtn);

        return crtn.handler;
    }
    public static StopCoroutine(handler: number) {
        for (let i = 0; i < this.crtns.length; i++) {
            if (this.crtns[i].handler === handler) {
                this.rmCrtn(i);
                return;
            }
        }
    }

    private static rmCrtn(index: number) {
        this.crtns.splice(index, 1);
    }
}

export type Coroutine = { generator: IterableIterator<any>, generatorWarpper: () => IterableIterator<any>, handler: number }